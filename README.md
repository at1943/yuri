yuri
-----

### 功能1：shiro Tag on freemarker

在freemarker中使用shiro tag， tag功能与jsp中的tag完全一致。

用法：在freemarkerVariables中增加配置

    <bean class="org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer">
		<property name="freemarkerVariables">
			<map>
				<entry key="shiro">
					<bean class="net.noday.yuri.shiro.freemarker.ShiroTags" />
				</entry>
			</map>
		</property>
	</bean>


freemarker模版中


<@shiro.guest></@shiro.guest>
<@shiro.user></@shiro.user>


### 功能2：shiro验证码登录封装


用法


    <bean id="shiroDbRealm" class="net.noday.yuri.shiro.ShiroDbRealm">
		<property name="service" ref="userServiceImpl"/>
	</bean>



    <bean id="shiroFilter" class="org.apache.shiro.spring.web.ShiroFilterFactoryBean">
		<property name="filters">
			<map>
				<entry key="authc">
					<bean class="net.noday.yuri.shiro.CaptchaFormAuthenticationFilter"></bean>
				</entry>
			</map>
		</property>
	</bean>




userServiceImpl需实现net.noday.yuri.shiro.auth.UserService

用户bean需实现net.noday.yuri.shiro.auth.Loginable



### 功能3：fastjson实现的spring jsonview


用法：配置view为<bean class="net.noday.yuri.web.json.MappingFastjsonJsonView" />


    <bean class="org.springframework.web.servlet.view.ContentNegotiatingViewResolver">
		<description></description>
		<property name="order" value="0" /><!-- 设置最高优先级 -->
		<property name="defaultContentType" value="text/html" /><!-- -->
		<property name="favorPathExtension" value="true" /><!-- 默认true，根据扩展名确定MIME类型 -->
		<property name="favorParameter" value="false" /><!-- 默认false，通过请求参数确定MIME类型，默认参数是formate，可通过parameterName属性改变 -->
		<property name="ignoreAcceptHeader" value="true" /><!-- 默认false，若以上步骤没确定MIME类型则根据请求报文头确定 -->
		<property name="mediaTypes">
			<description>favorPathExtension, favorParameter是true时才起作用</description>
			<map>
				<entry key="html" value="text/html" />
				<entry key="json" value="application/json" />
			</map>
		</property>
		<property name="defaultViews">
			<list>
				<bean class="net.noday.yuri.web.json.MappingFastjsonJsonView" />
			</list>
		</property>
	</bean>


