/**
 * 
 */
package net.noday.yuri.shiro.auth;

/**
 * @author Administrator
 *
 */
public interface Registable {

	void setSalt(String salt);
	
	String getPlainPassword();
	
	void setPassword(String password);
}
