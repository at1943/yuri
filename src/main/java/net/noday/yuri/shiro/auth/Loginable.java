/**
 * 
 */
package net.noday.yuri.shiro.auth;

import java.io.Serializable;
import java.util.Collection;

/**
 * yuri Loginable<T>
 *
 * @author <a href="http://www.noday.net">Noday</a>
 * @version , 2012-11-24
 * @since 
 * 
 */
public interface Loginable {
	
	Serializable getId();
	String getLoginName();
	String getName();
	String getPassword();
	String getSalt();
	String getRole();
	Collection<String> getRoles();
	Collection<String> getPermissions();
}
